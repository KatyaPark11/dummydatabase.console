﻿namespace DummyDatabase.Console
{
    public class ErrorsDisplayer
    {
        public enum ErrorIndices
        {
            ExtracurricularDataChangingIndex,
            NotEmptyFolderIndex,
            SelectedPlaceIndex,
            FileNameIndex,
            FileSysElemNameLenIndex,
            ProhibitedCharsIndex,
            FolderExistenceIndex,
            LinkExistenceIndex,
            ColumnTypeIndex,
            CountOfElementsIndex,
            PrimaryKeysCountIndex,
            KeysContradictionIndex,
            NotUniqueValuesIndex,
            SchemeExistenceIndex,
            DatabaseExistenceIndex,
            JsonFileIndex,
            SchemeElemsCountIndex,
            ForeignLinkAbsenceIndex,
            PrimaryLinkAbsenceIndex,
            LinkedFileExistenceIndex,
            LinkedFileExistenceInDatabaseIndex,
            LinkedTableColNameExistenceIndex,
            PrimaryKeyColValueExistenceIndex,
            UsedPrimaryValueRemovalIndex,
            EmptySelectedTableIndex,
            DefaultValueTypeIndex,
            EmptyColumnNameIndex,
            SameColumnNameIndex,
            DataAbsenceIndex
        }

        public static string ExtracurricularDataChangingErrorMessage()
        {
            return "This element wasn't found. It may have been modified outside of the application. " +
                   "We recommend that you reopen this database.";
        }

        public static string NotEmptyFolderErrorMessage()
        {
            return "The new database folder must be empty.";
        }

        public static string SelectedPlaceErrorMessage()
        {
            return "The new element can only be saved to a folder. Change the selected item.";
        }

        public static string FileNameErrorMessage()
        {
            return "This file already exists.";
        }

        public static string FileSysElemNameLenErrorMessage()
        {
            return "The entered name exceeds the maximum allowed length of the full path to the file (260) " +
                   "or the length of the name itself (255).";
        }

        public static string ProhibitedCharsErrorMessage()
        {
            return "This file name contains prohibited characters.";
        }

        public static string FolderExistenceErrorMessage()
        {
            return "This folder already exists.";
        }

        public static string LinkExistenceErrorMessage()
        {
            return "This table contains at least one linked foreign table, so you can't remove it.";
        }

        public static string ColumnTypeErrorMessage(int row, int column, string element, string type)
        {
            return $"There is an error in {row} row and {column} column: Cannot convert \"{element}\" to \"{type}\".";
        }

        public static string CountOfElementsErrorMessage(int row, int rightCount, int wrongCount)
        {
            return $"There is an error in {row} row:  The count of elements must be {rightCount} but it was {wrongCount}.";
        }

        public static string PrimaryKeysCountErrorMessage()
        {
            return "There can be only one column with a primary key in the table.";
        }

        public static string KeysContradictionErrorMessage()
        {
            return "A scheme column cannot have a foreign key and a primary key at the same time.";
        }

        public static string NotUniqueValuesErrorMessage()
        {
            return "Values ​​in a column with the IsPrimary flag should be unique, but this is not the case here.";
        }

        public static string SchemeExistenceErrorMessage(string schemePath)
        {
            return $"There is no file \"{schemePath}\".";
        }

        public static string DatabaseExistenceErrorMessage(string databasePath)
        {
            return $"The database \"{databasePath}\" doesn't exist.";
        }

        public static string JsonFileErrorMessage()
        {
            return "The scheme of this file is invalid. Make sure the text in this is in json format.";
        }

        public static string SchemeElemsCountErrorMessage() 
        {
            return "Not all elements of this scheme contain the parameters required by the program " +
                   "(For every \"column\": \"name\" and \"type\" are necessary, \"isPrimaryKey\", \"foreignTables\", \"isForeignKey\" " +
                   "and \"primaryTable\" are not. For every scheme: \"databasePath\" is necessary.)";
        }

        public static string ForeignLinkAbsenceErrorMessage()
        {
            return "There is \"IsForeignKey\" = \"true\", but there is no valid \"primaryTable\".";
        }

        public static string PrimaryLinkAbsenceErrorMessage()
        {
            return "There is \"IsPrimaryKey\" = \"true\", but not every object in \"foreignTables\" is valid.";
        }

        public static string LinkedFileExistenceErrorMessage(string path)
        {
            return $"There is no file \"{path}\".";
        }

        public static string LinkedFileExistenceInDatabaseErrorMessage(string path)
        {
            return $"There is no file \"{path}\" in current database.";
        }

        public static string LinkedTableColNameExistenceErrorMessage(string path, string columnName)
        {
            return $"The file \"{path}\" doesn't contain column name \"{columnName}\".";
        }

        public static string PrimaryKeyColValueExistenceErrorMessage(string path, string columnName, object value)
        {
            return $"The file \"{path}\" doesn't contain \"{value}\" in column \"{columnName}\".";
        }

        public static string UsedPrimaryValueRemovalErrorMessage(string columnName, object value)
        {
            return $"The \"{value}\" in column \"{columnName}\" is used in linked column, so you can't remove this row.";
        }

        public static string EmptySelectedTableErrorMessage()
        {
            return "The primary table must have at least one row.";
        }

        public static string DefaultValueTypeErrorMessage(string element, string type)
        {
            return $"Cannot convert element \"{element}\" to {type}.";
        }

        public static string EmptyColumnNameErrorMessage()
        {
            return "The column name must have at least one character.";
        }

        public static string SameColumnNameErrorMessage()
        {
            return "Column headers cannot match.";
        }

        public static string DataAbsenceErrorMessage(List<string> missingData)
        {
            string res = "";
            foreach (string data in missingData)
            {
                res += $"{data}, ";
            }
            return $"{res.Remove(res.Length - 2)} not specified.";
        }
    }
}
