﻿namespace DummyDatabase.Console
{
    public class Error
    {
        public ErrorsDisplayer.ErrorIndices Index { get; private set; }
        public string Message { get; private set; }
        public string Caption { get; private set; }

        public Error(int index, params object[] parameters) 
        {
            Index = (ErrorsDisplayer.ErrorIndices)index;
            switch (index)
            {
                case (int)ErrorsDisplayer.ErrorIndices.ExtracurricularDataChangingIndex:
                {
                    Message = ErrorsDisplayer.ExtracurricularDataChangingErrorMessage();
                    Caption = "THE ELEMENT DOESN'T EXIST";
                    break;
                }
                case (int)ErrorsDisplayer.ErrorIndices.NotEmptyFolderIndex:
                {
                    Message = ErrorsDisplayer.NotEmptyFolderErrorMessage();
                    Caption = "ISN'T EMPTY FOLDER";
                    break;
                }
                case (int)ErrorsDisplayer.ErrorIndices.SelectedPlaceIndex:
                {
                    Message = ErrorsDisplayer.SelectedPlaceErrorMessage();
                    Caption = "WRONG SELECTED ITEM";
                    break;
                }
                case (int)ErrorsDisplayer.ErrorIndices.FileNameIndex:
                {
                    Message = ErrorsDisplayer.FileNameErrorMessage();
                    Caption = "THE SAME FILE NAME";
                    break;
                }
                case (int)ErrorsDisplayer.ErrorIndices.FileSysElemNameLenIndex:
                {
                    Message = ErrorsDisplayer.FileSysElemNameLenErrorMessage();
                    Caption = "INVALID LENGTH";
                    break;
                }
                case (int)ErrorsDisplayer.ErrorIndices.ProhibitedCharsIndex:
                {
                    Message = ErrorsDisplayer.ProhibitedCharsErrorMessage();
                    Caption = "INVALID FILE SYSTEM ELEMENT NAME";
                    break;
                }
                case (int)ErrorsDisplayer.ErrorIndices.FolderExistenceIndex:
                {
                    Message = ErrorsDisplayer.FolderExistenceErrorMessage();
                    Caption = "THE SAME FOLDER NAME";
                    break;
                }
                case (int)ErrorsDisplayer.ErrorIndices.LinkExistenceIndex:
                {
                    Message = ErrorsDisplayer.LinkExistenceErrorMessage();
                    Caption = "LINKED FOREIGN TABLE EXISTENCE";
                    break;
                }
                case (int)ErrorsDisplayer.ErrorIndices.ColumnTypeIndex:
                {
                    string tableName = parameters[0].ToString().ToUpperInvariant();
                    int row = Int32.Parse(parameters[1].ToString());
                    int column = Int32.Parse(parameters[2].ToString());
                    string element = parameters[3].ToString();
                    string type = parameters[4].ToString();
                    Message = ErrorsDisplayer.ColumnTypeErrorMessage(row, column, element, type);
                    Caption = $"{tableName}: ISN'T APPROPRIATE TO SCHEME";
                    break;
                }  
                case (int)ErrorsDisplayer.ErrorIndices.CountOfElementsIndex:
                {
                    string tableName = parameters[0].ToString().ToUpperInvariant();
                    int row = int.Parse(parameters[1].ToString());
                    int rightCount = Int32.Parse(parameters[2].ToString());
                    int wrongCount = Int32.Parse(parameters[3].ToString());
                    Message = ErrorsDisplayer.CountOfElementsErrorMessage(row, rightCount, wrongCount);
                    Caption = $"{tableName}: ISN'T APPROPRIATE TO SCHEME";
                    break;
                }
                case (int)ErrorsDisplayer.ErrorIndices.PrimaryKeysCountIndex:
                {
                    string tableName = parameters[0].ToString().ToUpperInvariant();
                    Message = ErrorsDisplayer.PrimaryKeysCountErrorMessage();
                    Caption = $"{tableName}: MORE THAN ONE PRIMARY KEY";
                    break; 
                }
                case (int)ErrorsDisplayer.ErrorIndices.KeysContradictionIndex:
                {
                    string tableName = parameters[0].ToString().ToUpperInvariant();
                    Message = ErrorsDisplayer.KeysContradictionErrorMessage();
                    Caption = $"{tableName}: KEYS CONTRADICTION";
                    break; 
                }
                case (int)ErrorsDisplayer.ErrorIndices.NotUniqueValuesIndex:
                {
                    string tableName = parameters[0].ToString().ToUpperInvariant();
                    Message = ErrorsDisplayer.NotUniqueValuesErrorMessage();
                    Caption = $"{tableName}: NOT UNIQUE VALUES";
                    break; 
                }
                case (int)ErrorsDisplayer.ErrorIndices.SchemeExistenceIndex:
                {
                    string tableName = parameters[0].ToString().ToUpperInvariant();
                    string schemePath = parameters[1].ToString();
                    Message = ErrorsDisplayer.SchemeExistenceErrorMessage(schemePath);
                    Caption = $"{tableName}: SCHEME FILE ABSENCE";
                    break;
                }
                case (int)ErrorsDisplayer.ErrorIndices.DatabaseExistenceIndex:
                {
                    string tableName = parameters[0].ToString().ToUpperInvariant();
                    string databasePath = parameters[1].ToString();
                    Message = ErrorsDisplayer.DatabaseExistenceErrorMessage(databasePath);
                    Caption = $"{tableName}: SCHEME FILE ABSENCE";
                    break;
                }
                case (int)ErrorsDisplayer.ErrorIndices.JsonFileIndex:
                {
                    string tableName = parameters[0].ToString().ToUpperInvariant();
                    Message = ErrorsDisplayer.JsonFileErrorMessage();
                    Caption = $"{tableName}: INVALID JSON FILE";
                    break;
                }
                case (int)ErrorsDisplayer.ErrorIndices.SchemeElemsCountIndex:
                {
                    string tableName = parameters[0].ToString().ToUpperInvariant();
                    Message = ErrorsDisplayer.SchemeElemsCountErrorMessage();
                    Caption = $"{tableName}: INVALID SCHEME FILE";
                    break;
                }
                case (int)ErrorsDisplayer.ErrorIndices.ForeignLinkAbsenceIndex:
                {
                    string tableName = parameters[0].ToString().ToUpperInvariant();
                    Message = ErrorsDisplayer.ForeignLinkAbsenceErrorMessage();
                    Caption = $"{tableName}: FOREIGN LINK ABSENCE";
                    break;
                }
                case (int)ErrorsDisplayer.ErrorIndices.PrimaryLinkAbsenceIndex:
                {
                    string tableName = parameters[0].ToString().ToUpperInvariant();
                    Message = ErrorsDisplayer.PrimaryLinkAbsenceErrorMessage();
                    Caption = $"{tableName}: PRIMARY LINK ABSENCE";
                    break;
                }
                case (int)ErrorsDisplayer.ErrorIndices.LinkedFileExistenceIndex:
                {
                    string tableName = parameters[0].ToString().ToUpperInvariant();
                    string path = parameters[1].ToString();
                    Message = ErrorsDisplayer.LinkedFileExistenceErrorMessage(path);
                    Caption = $"{tableName}: LINKED FILE ABSENCE";
                    break;
                }
                case (int)ErrorsDisplayer.ErrorIndices.LinkedFileExistenceInDatabaseIndex:
                {
                    string tableName = parameters[0].ToString().ToUpperInvariant();
                    string path = parameters[1].ToString();
                    Message = ErrorsDisplayer.LinkedFileExistenceInDatabaseErrorMessage(path);
                    Caption = $"{tableName}: LINKED FILE ABSENCE IN DATABASE";
                    break;
                }
                case (int)ErrorsDisplayer.ErrorIndices.LinkedTableColNameExistenceIndex:
                {
                    string tableName = parameters[0].ToString().ToUpperInvariant();
                    string path = parameters[1].ToString();
                    string columnName = parameters[2].ToString();
                    Message = ErrorsDisplayer.LinkedTableColNameExistenceErrorMessage(path, columnName);
                    Caption = $"{tableName}: COLUMN NAME ABSENCE IN LINKED TABLE";
                    break;
                }
                case (int)ErrorsDisplayer.ErrorIndices.PrimaryKeyColValueExistenceIndex:
                {
                    string tableName = parameters[0].ToString().ToUpperInvariant();
                    string path = parameters[1].ToString();
                    string columnName = parameters[2].ToString();
                    object value = parameters[3];
                    Message = ErrorsDisplayer.PrimaryKeyColValueExistenceErrorMessage(path, columnName, value);
                    Caption = $"{tableName}: VALUE ABSENCE IN PRIMARY TABLE";
                    break;
                }
                case (int)ErrorsDisplayer.ErrorIndices.UsedPrimaryValueRemovalIndex:
                {
                    string tableName = parameters[0].ToString().ToUpperInvariant();
                    string columnName = parameters[1].ToString();
                    object value = parameters[2];
                    Message = ErrorsDisplayer.UsedPrimaryValueRemovalErrorMessage(columnName, value);
                    Caption = $"{tableName}: VALUE ABSENCE IN PRIMARY TABLE";
                    break;
                }
                case (int)ErrorsDisplayer.ErrorIndices.EmptySelectedTableIndex:
                {
                    string tableName = parameters[0].ToString().ToUpperInvariant();
                    Message = ErrorsDisplayer.EmptySelectedTableErrorMessage();
                    Caption = $"{tableName}: EMPTY SELECTED TABLE";
                    break;
                }
                case (int)ErrorsDisplayer.ErrorIndices.DefaultValueTypeIndex:
                {
                    string element = parameters[0].ToString();
                    string type = parameters[1].ToString();
                    Message = ErrorsDisplayer.DefaultValueTypeErrorMessage(element, type);
                    Caption = "WRONG DEFAULT VALUE";
                    break;
                }
                case (int)ErrorsDisplayer.ErrorIndices.EmptyColumnNameIndex:
                {
                    Message = ErrorsDisplayer.EmptyColumnNameErrorMessage();
                    Caption = "THE EMPTY COLUMN NAME";
                    break;
                }
                case (int)ErrorsDisplayer.ErrorIndices.SameColumnNameIndex:
                {
                    Message = ErrorsDisplayer.SameColumnNameErrorMessage();
                    Caption = "THE SAME COLUMN NAME";
                    break;
                }
                case (int)ErrorsDisplayer.ErrorIndices.DataAbsenceIndex:
                {
                    List<string> missingData = parameters[0] as List<string>;
                    Message = ErrorsDisplayer.DataAbsenceErrorMessage(missingData);
                    Caption = "DATA ABSENCE";
                    break;
                }
            }
        }
    }
}
